import React from 'react';
import { Accordion, AccordionItem } from 'react-sanfona';


export const createProficienciesList = (proficiencies) => {
    return (
        <Accordion>
            {<AccordionItem className="accordion" title={"Proficiencies ↓"} expanded="true">
                <div>
                    {proficiencies.map(item => { return (<li key={item} className="LanguageProficienciesList">{item}</li>)})}
                </div>
            </AccordionItem>}
        </Accordion>);
};
